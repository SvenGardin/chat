package main.java.org.javaee7.websocket.chat;

import java.io.IOException;
import java.util.logging.Logger;

import javax.websocket.CloseReason;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * @author Arun Gupta
 */
@ServerEndpoint("/chat")
public class ChatEndpoint {

    Logger logger = Logger.getLogger(ChatEndpoint.class.getName());

    @OnMessage
    public void message(String message, Session client) throws IOException, EncodeException {
	final String incomingClientId = client.getId();
	this.logger.fine("Websocket client id : " + incomingClientId + " sent " + message);

	for (final Session peer : client.getOpenSessions()) {
	    final String outgoingClientId = peer.getId();
	    String outgoingMessage = message;
	    if (incomingClientId.equals(outgoingClientId)) {
		final String subMessages[] = message.split(":");
		if (subMessages.length == 2) {
		    outgoingMessage = "I said: " + subMessages[1];
		}
	    }
	    final Basic peerEndPoint = peer.getBasicRemote();
	    peerEndPoint.sendText(outgoingMessage);
	    this.logger.fine("Sent message '" + outgoingMessage + "' to " + outgoingClientId);

	}
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
	final String sessionId = session.getId();
	this.logger.fine("onClose called from " + sessionId);
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig endpointConfig) {
	final String sessionId = session.getId();
	this.logger.fine("onOpen called from " + sessionId);
    }
}
